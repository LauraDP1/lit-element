import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {


  static get properties() {
    return {
      peopleStats: {type: Object},
      yearsFilter: {type:Number}
    }
  }
  constructor() {
    super();

    this.peopleStats = {};
    

  }

  




  render() {
     return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <aside>
          </section>
          <div class="mt-5"> 
          </br>
          </div> Hay <span class="badge badge-pill badge-primary">${this.peopleStats.numberOfPeople}</span>
            <div class="mt-5">
            <div>
            <input type="range" min="0" max="${this.peopleStats.maxYerarsInCompany}"
            step="1"
            value="${this.peopleStats.maxYerarsInCompany}"
            @input=${this.updateMaxYearsInCompanyFilter}"
            />

            <button class="w-50 btn btn-success" style="font-size": 50px" @click="${this.newPerson}"><strong>+</strong></button>
            </div>
            </div>
            </section>
            </aside>
  `;
    }

    newPerson(e) {
      console.log("newPerson en persona-sidebar");
      console.log("Se va a crear una persona nueva");
      this.dispatchEvent(new CustomEvent("new-person", {}));
    
    }

    updateMaxYearsInCompanyFilter(e) {

      this.dispatchEvent(
        new CustomEvent("updated-max-years-filter",
        {
          detail: {
            maxYearsInCompany: e.target.value
          }
        }
        )
    
      );
    }

}
customElements.define('persona-sidebar', PersonaSidebar)

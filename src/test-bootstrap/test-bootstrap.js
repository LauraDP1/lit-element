
import { LitElement, html, css } from 'lit-element';

class TestBootstrap extends LitElement {
 
    static get styles() {
      return css`
      .redbg {
          background-color: red;
        }
        .greenbg {
        background-color: green;
      }
      .greybg {
        background-color: grey;
      }
      .bluebg {
        background-color: blue;
      }
        `;
      }
  render() {
     return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <h3>Test Bootstrap</h3>
        <div class="row greybg">
        <div class="col-2 offst-1 redbg">Col 1</div>
        <div class="col greenbg">Col 2</div>
        <div class="col greybg">Col 2</div>
        <div class="col bluebg">Col 4 </div>
  `;
  
    }
}
customElements.define('test-bootstrap', TestBootstrap)

import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

  static get properties() {
    return {
      person: {type: Object},
      editingPerson: {type: Boolean}
    }

  } 

  constructor() {
    super();
    this.resetFormData();

  }



  render() {
     return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
     <div>
      <form>
  
        <div class="form-group">
        <label>Nombre completo</label>
        <input type="text" id="personFormName" class="form-control" placeholder="Nombre Completo"
           @input="${this.updateName}"
           .value="${this.person.name}"
           ?disabled="${this.editingPerson}"/>
        </div>

        <div class="form-group">
        <label>Perfil</label>
        <textarea class="form-control" placeholder="Perfil" rows="5"
        @input="${this.updateProfile}"
        .value="${this.person.profile}"></textarea>
        </div>

        <div class="form-group">
        <label>Años en la empresa</label>
        <input type="text" class="form-control" placeholder="Años en la empresa" 
        @input="${this.updateYearsInCompany}"
        .value="${this.person.yearsInCompany}"/>
        </div>  
                <button class="btn btn-primary" @click="${this.goBack}"><strong>Atras</strong></button>
                <button class="btn btn-success" @click="${this.storePerson}"> <strong>Guardar</strong></button>
                </form>
                </div> 
                `;


 
    }
    

    updateName(e) {
      console.log("updateName");
      console.log("Actualizado la propiedad name con el valor " + e.target.value);
      this.person.name = e.target.value;
    }

    updateProfile(e) {
      console.log("updateProfile");
      console.log("Actualizado la propiedad profile con el valor " + e.target.value);
      this.person.profile= e.target.value;
    }

    updateYearsInCompany(e) {
      console.log("updateYearsInCompany");
      console.log("Actualizado la propiedad years in company con el valor " + e.target.value);
      this.person.yearsInCompany = e.target.value;
    }


  goBack(e) {
    console.log("goBack");
    e.prevenDefault();

    this.dispatchEvent(new CustomEvent("persona-form-close",{}));
    this.resetFormData();


  }

  storePerson(e) {
    console.log("storePerson");
    e.preventDefault();

    this.person.photo =  {
      src: "./img/persona.jpg",
      alt: "Persona"
    };

    console.log("La propiedad name vale " + this.person.name);
    console.log("La propiedad profile vale " + this.person.profile);
    console.log("La propiedad yearsInCompany vale " + this.person.yearsInCompany);

    this.dispatchEvent(new CustomEvent("persona-form-store", {
        detail: {
            person: {
              name: this.person.name,
              profile:  this.person.profile,
              yearsInCompany: this.person.yearsInCompany,
              photo: this.person.photo
            },
            editingPerson: this.editingPerson
            
          }
        }
        ));
        console.log("valorde editingPerson" + this.editingPerson);
        this.resetFormData();
       
      
  }

  resetFormData(){
    console.log("resetFormData");
    this.person ={};
    this.person.name = "";
    this.person.profile = "";
    this.person.yearsInCompany = "";

    this.editingPerson = false;
  }

  
}
customElements.define('persona-form', PersonaForm)


import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

  static get properties() {
    return {
      fname: {type: String},
      yearsInCompany: {type: Number},
      profile: {type: String},
      photo: {type: Object}
    }
  }
  render() {
     return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
     <div class="card">
     <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}" height="100" width="50" class="card-img-top>
      <div class="card-body">
        <h5 classs="card-title">${this.fname}</h5>
        <u1 class="list-group list-group-flush">
          <li class="list-group-item">${this.yearsInCompany} Años en la empresa </li>
          <li class="list-group-item">${this.profile}  </li>
          </u1>

     </div>
     <div class="card-footer">
      <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>x</strong></button>
      <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"><strong>Info</strong></button>

      </div>

      
      </div>
  `;
    }

    deletePerson(e) 
    {
      console.log("deletePerson en persona-ficha-listado");
      console.log("se va a borrar la persona nombre " + this.fname);

      this.dispatchEvent(
          new CustomEvent("delete-person",
            {
              detail: {
                  name:this.fname
                }
              }

          )
      );
    }

    moreInfo(e) 

    {

      console.log("moreInfo");
      console.log("Se ha pedido informacide la persona " + this.fname);

      this.dispatchEvent(
          new CustomEvent("info-person", {
            detail: {
                name:this.fname
              }
            })
      );
    }

}
customElements.define('persona-ficha-listado', PersonaFichaListado)
